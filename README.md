Add some service to easily change views.
Services:
* views_cleanup.filter_cleanup
  ```
  $filter_check_options = ['entity_field' => 'field_is_shared','operator' => '!=',];
  $views_ids = ['views_id1','views_id2']; 
  \Drupal::service('views_cleanup.filter_cleanup')->cleanupViewsFiltersByFilterCheckOptions($filter_check_options, $views_ids);// $views_ids is optional, NUll means clean all views
  ```
* views_cleanup.filter_replacement
* views_cleanup.filter_add
* views_cleanup.aggregate_views_filter_option
* views_cleanup.denpendencies
  ```
  $modules = ['module_name1', 'module_name2']
  \Drupal::service('views_cleanup.denpendencies')->cleanupViewsDependencyModule($modules, $views_ids);// $views_ids is optional, NUll means clean all views
  ```
* views_cleanup.fields_cleanup