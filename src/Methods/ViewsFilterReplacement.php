<?php

namespace Drupal\views_cleanup\Methods;

use Drupal\views\Entity\View;
use Drupal\views_cleanup\ViewsCleanupBase;

class ViewsFilterReplacement extends ViewsCleanupBase {

  /**
   * @param array $filterCheckOptions
   *     like: ['entity_field' => 'field_is_shared','operator' => '!=',]
   * @param string $new_filter_name
   * @param array $new_filter_config
   * @param string|NULL $display_id
   * @param array|NULL $views_ids
   *
   * @return void
   */
  public function replaceViewsFilterByFilterCheckOptions(array $filterCheckOptions, string $new_filter_name, array $new_filter_config, string $display_id = NULL, array $views_ids = NULL) {
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $config = \Drupal::configFactory()->getEditable('views.view.' . $view_id);
      $displays = $config->get('display');
      $display_needs_update = FALSE;
      foreach ($displays as $the_display_id => $display) {
        if (!empty($display_id) && $the_display_id != $display_id) {
          continue;
        }
        if (!isset($display['display_options']['filters'])) {
          continue;
        }
        // Check views filter
        $filter_needs_update = FALSE;
        $new_display_filters = [];
        foreach ($display['display_options']['filters'] as $filter_name => $filter) {
          $filter_check_result = $this->filterCheckOptions($filterCheckOptions, $filter);

          if ($filter_check_result) {
            $filter_needs_update = TRUE;
            if (!isset($new_display_filters[$new_filter_name])) {
                $new_display_filters[$new_filter_name] = $new_filter_config;
            }
            // Use continue to skip the original filter be added to the $new_display_filters;
            continue;
          }
          $new_display_filters[$filter_name] = $filter;
        }
        if ($filter_needs_update) {
          $display_needs_update = TRUE;
          $displays[$the_display_id]['display_options']['filters'] = $new_display_filters;
        }
        // End check views filter
      }
      if ($display_needs_update) {
        $config->set('display', $displays);
        $config->save(TRUE);
      }
    }
  }


  public static function replaceViewsFilterByFilterName(array $original_filter_name, string $new_filter_name, array $new_filter_config, string $views_display_id = NULL, array $views_ids = NULL) {
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $config = \Drupal::configFactory()->getEditable('views.view.' . $view_id);
      $displays = $config->get('display');
      $display_needs_update = FALSE;
      foreach ($displays as $display_id => $display) {
        if (!empty($views_display_id) && $display_id != $views_display_id) {
          continue;
        }
        if (!isset($display['display_options']['filters'])) {
          continue;
        }
        // Check views filter
        $filter_needs_update = FALSE;
        $new_display_filters = [];
        foreach ($display['display_options']['filters'] as $filter_name => $filter) {

          if ($original_filter_name == $filter_name) {
            $filter_needs_update = TRUE;
            if (!isset($new_display_filters[$new_filter_name])) {
              $new_display_filters[$new_filter_name] = $new_filter_config;
            }
            // Use continue to skip the original filter be added to the $new_display_filters;
            continue;
          }
          $new_display_filters[$filter_name] = $filter;
        }
        if ($filter_needs_update) {
          $display_needs_update = TRUE;
          $displays[$display_id]['display_options']['filters'] = $new_display_filters;
        }
        // End check views filter
      }
      if ($display_needs_update) {
        $config->set('display', $displays);
        $config->save(TRUE);
      }
    }
  }
}