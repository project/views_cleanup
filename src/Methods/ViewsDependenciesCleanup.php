<?php

namespace Drupal\views_cleanup\Methods;

use Drupal\views\Entity\View;

class ViewsDependenciesCleanup {

  public static function cleanupViewsDependencyModules(array $modules, array $views_ids = NULL) {
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $config = \Drupal::configFactory()
        ->getEditable('views.view.' . $view_id);
      $view_needs_update = FALSE;

      $dependencies = $config->get('dependencies');
      if (isset($dependencies['module'])) {
        foreach ($dependencies['module'] as $key => $value) {
          foreach ($modules as $module) {
            if ($value == $module) {
              unset($dependencies['module'][$key]);
              $view_needs_update = TRUE;
            }
          }
        }
      }

      if ($view_needs_update) {
        $config->set('dependencies', $dependencies);
        $config->save(TRUE);
      }
    }
  }

}