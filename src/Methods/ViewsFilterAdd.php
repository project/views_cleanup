<?php

namespace Drupal\views_cleanup\Methods;

use Drupal\views\Entity\View;
use Drupal\views_cleanup\ViewsCleanupBase;

class ViewsFilterAdd extends ViewsCleanupBase {

  public static function addViewsFilter(string $filter_name, array $filterConfig, string  $views_display_id = NULL, array $views_ids = NULL) {
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $config = \Drupal::configFactory()
        ->getEditable('views.view.' . $view_id);
      $view_needs_update = FALSE;

      // Clean Displays
      $displays = $config->get('display');

      // *** Cleanup filters by $field_name
      foreach ($displays as $display_id => $display) {
        if (!empty($views_display_id) && $display_id != $views_display_id) {
          continue;
        }
        if (!isset($display['display_options']['filters'])) {
          continue;
        }

        if (isset($display['display_options']['filters'][$filter_name])) {
          continue;
        }
        $displays[$display_id]['display_options']['filters'][$filter_name] = $filterConfig;
        $view_needs_update = TRUE;
      }

      if ($view_needs_update) {
        $config->set('display', $displays);
        $config->save(TRUE);
      }
    }
  }

}