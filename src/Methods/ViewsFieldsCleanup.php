<?php

namespace Drupal\views_cleanup\Methods;

use Drupal\views\Entity\View;

class ViewsFieldsCleanup {

  /**
   * @param array $views_field_plugins
   * Example:
   * [
   *  'entity_current_state',
   *  'entity_label_with_workspace',
   *  'entity_revision_link_with_workspace',
   *  'workspace_archived',
   *  'workspace_archived_link',
   *  'workspace_description',
   *  'workspace_is_default',
   * ];
   * @param array|NULL $views_ids
   *
   * @return void
   */
  public static function cleanupViewsFieldsByViewsFieldPlugins(array $views_field_plugins, string $views_display_id = NULL, array $views_ids = NULL) {
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $view_needs_update = FALSE;

      /** @var \Drupal\Core\Config\Config $config */
      $config = \Drupal::configFactory()
        ->getEditable('views.view.' . $view_id);

      // Clean display fields
      $displays = $config->get('display');
      $display_needs_update = FALSE;
      foreach ($views_field_plugins as $views_field_plugin) {
        foreach ($displays as $display_id => $display) {
          if (!empty($views_display_id) && $display_id != $views_display_id) {
            continue;
          }
          if (!isset($display['display_options']['fields'])) {
            continue;
          }
          foreach ($display['display_options']['fields'] as $field_name => $field) {
            if ($field['plugin_id'] == $views_field_plugin) {
              unset($displays[$display_id]['display_options']['fields'][$field_name]);
              unset($displays[$display_id]['display_options']['style']['options']['columns'][$views_field_plugin]);
              unset($displays[$display_id]['display_options']['style']['options']['info'][$views_field_plugin]);
              $display_needs_update = TRUE;
            }
          }
        }
      }

      if ($display_needs_update) {
        $config->set('display', $displays);
        $view_needs_update = TRUE;
      }

      if ($view_needs_update) {
        $config->save(TRUE);
      }
    }
  }

}