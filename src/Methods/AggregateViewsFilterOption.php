<?php

namespace Drupal\views_cleanup\Methods;

use Drupal\views\Entity\View;
use Drupal\views_cleanup\ViewsCleanupBase;

class AggregateViewsFilterOption extends ViewsCleanupBase {

  /**
   * @param string $option
   *  like 'field','entity_field','operator'
   * @param array $filterCheckOptions
   *  like: ['entity_field' => 'field_is_shared','operator' => '!=',]
   * @param array|NULL $views_ids
   *
   * @return array
   */
  public function aggregateViewsFilterOptionByFilterCheckOptions(string $option, array $filterCheckOptions, array $views_ids = NULL) {
    $option_values = [];
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $config = \Drupal::configFactory()
        ->getEditable('views.view.' . $view_id);

      // Clean Displays
      $displays = $config->get('display');

      // *** Cleanup filters by $field_name
      foreach ($displays as $display_id => $display) {
        if (!isset($display['display_options']['filters'])) {
          continue;
        }

        foreach ($display['display_options']['filters'] as $filter) {
          $filter_check_result = $this->filterCheckOptions($filterCheckOptions, $filter);
          if ($filter_check_result) {
            if (isset($filter[$option])) {
              $option_values[$filter[$option]] = $filter[$option];
            }
          }
        }
      }
    }
    return $option_values;
  }

}