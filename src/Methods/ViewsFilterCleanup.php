<?php

namespace Drupal\views_cleanup\Methods;

use Drupal\views\Entity\View;
use Drupal\views_cleanup\ViewsCleanupBase;

class ViewsFilterCleanup extends ViewsCleanupBase {

  public function cleanupViewsFiltersByFilterName($filter_name, array $views_ids = NULL) {
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $config = \Drupal::configFactory()
        ->getEditable('views.view.' . $view_id);
      $view_needs_update = FALSE;

      $displays = $config->get('display');
      foreach ($displays as $display_id => $display) {
        if (!isset($display['display_options']['filters'])) {
          continue;
        }

        if (isset($display['display_options']['filters'][$filter_name])) {
          unset($displays[$display_id]['display_options']['filters'][$filter_name]);
          $view_needs_update = TRUE;
        }
      }

      if ($view_needs_update) {
        $config->set('display', $displays);
        $config->save(TRUE);
      }
    }
  }

  /**
   * @param array $filterCheckOptions
   * like: ['entity_field' => 'field_is_shared','operator' => '!=',]
   * @param array|NULL $views_ids
   *
   * @return void
   */
  public function cleanupViewsFiltersByFilterCheckOptions(array $filterCheckOptions, $views_display_id = NULL, array $views_ids = NULL) {
    $views = View::loadMultiple($views_ids);
    foreach ($views as $view) {
      $view_id = $view->id();
      $config = \Drupal::configFactory()
        ->getEditable('views.view.' . $view_id);
      $view_needs_update = FALSE;

      // Clean Displays
      $displays = $config->get('display');

      // *** Cleanup filters by $field_name
      foreach ($displays as $display_id => $display) {
        if (!empty($views_display_id) && $display_id != $views_display_id) {
          continue;
        }
        if (!isset($display['display_options']['filters'])) {
          continue;
        }

        // Check views filter
        foreach ($display['display_options']['filters'] as $filter_name => $filter) {
          $filter_check_result = $this->filterCheckOptions($filterCheckOptions, $filter);
          if ($filter_check_result) {
            unset($displays[$display_id]['display_options']['filters'][$filter_name]);
            $view_needs_update = TRUE;
          }
        }
      }

      if ($view_needs_update) {
        $config->set('display', $displays);
        $config->save(TRUE);
      }
    }
  }

}