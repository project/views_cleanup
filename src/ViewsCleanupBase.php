<?php

namespace Drupal\views_cleanup;

class ViewsCleanupBase {

  /**
   * Check if the views filter matching the provide check options
   *
   * @param array $options Check options
   * @param array $filter views filter
   *
   * @return bool
   */
  protected function filterCheckOptions(array $options, array $filter): bool {
    foreach ($options as $key => $value) {
      if (!isset($filter[$key])) {
        return FALSE;
      }
      if ($filter[$key] != $value) {
        return FALSE;
      }
    }
    return TRUE;
  }

}