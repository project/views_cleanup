<?php

namespace Drupal\views_cleanup\Drush\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class ViewsCleanupCommands extends DrushCommands {

  /**
   * delete views Filters by FilterName for all views.
   *
   * @command views-cleanup:delete-filter-by-filter-name
   *
   * @param string $filter_name
   *
   * @usage vcdfbfn uuid
   *   Delete the filter uuid for all views
   */
  public function deleteFiltersbyFilterName($filter_name) {
    \Drupal::service('views_cleanup.filter_cleanup')
      ->cleanupViewsFiltersByFilterName($filter_name);
  }

  /**
   * delete views Filters by Field Name for all views.
   *
   * @command views-cleanup:delete-filter-by-field-name
   *
   * @param string $field_name
   *
   */
  public function deleteFiltersbyFieldName($field_name) {
    \Drupal::service('views_cleanup.filter_cleanup')
      ->cleanupViewsFiltersByFilterCheckOptions(['entity_field' => $field_name]);
  }

}

